from InternalDataContracts.genre_link_collection import GenreLinkCollection


class DataCollection:
    @staticmethod
    def extract_data():
        return [
            GenreLinkCollection(genre_name='Reggae', song_youtube_ids=[
                'K6oYyG0KcvQ',
                'Oe-BV7OYepI',
                'Mwo33Sy0vWk',
                '0k27suBmV0U',
                'vdB-8eLEW8g',
                'QI-m8z6Ibws',
                'K-XmNm0inyM',
                'tm9KHjxSzxw',
                'pHlSE9j5FGY',
                'WT4iJ2jZv7M',
                'DMkasTMiJBI',
                'RCHTAu4In70',
                '6wiEjEUMaUA',
                'qP3VsZ7QD08',
                'tgGoNzVfNf8',
                'PMkhsiwgh4M',
                'wlqB13iJu2o',
                '_4jIlm8WvIo',
                'FL_BshgpNmo',
                'rYeWnbmFUhQ',
                'VpGKcyL_0KQ',
                'yDuQBZqQL7g',
                't4tgyyDKhsM',
                'jHUQg5rvBEg',
                'sJW-aocSxa0',
                'scs6mWiHnyA',
                '7faOF6C8bZg',
                '9Lz6HPz9FCk',
                'I95IsZFcdCk',
                'mDODX5_n3Us'
            ]),
            GenreLinkCollection(genre_name='Rock', song_youtube_ids=[
                '1w7OgIMMRc4',
                'hTWKbfoikeg',
                'djV11Xbc914',
                '9jK-NcRmVcw',
                'OMOGaugKpzs',
                'OKvCV8MFIaw',
                'btPJPFnesV4',
                'Rbm6GXllBiw',
                '5IpYOF4Hi6Q',
                'ASQTE9axcxE',
                'O9TunCtR3dQ',
                'qN5zw04WxCc',
                'pAgnJDJN4VA',
                'aSNJ00iAZ7I',
                'ZiRuj2_czzw',
                'r5EXKDlf44M',
                '6dc56W_bnyU',
                'ohOtDA3dTAA',
                'aeZnkEMIxBM',
                '4YSYDYiKmGA',
                'ldNLO7pvmgo',
                'J-0KRM_eROA',
                'sfR_HWMzgyc',
                'Qq4j1LtCdww',
                'xfWPqRtozh0',
                'Rbm6GXllBiw',
                'SVFU4qc7nhs',
                '3T1c7GkzRQQ',
                'pkcJEvMcnEg',
                'NbOCF9zYgHc'
            ]),
            GenreLinkCollection(genre_name='Electronic', song_youtube_ids=[
                '_ovdm2yX4MA',
                'p-Z3YrHJ1sU',
                'DBRf2Wc3bRw',
                'vuJ2V-9GhBo',
                'Dr1nN__-2Po',
                '_o-XIryB2gg',
                'QLoUH6LN9lk',
                'McEoTIqoRKk',
                'lETmskoqh30',
                'Wb5VOQexMBU',
                'EpbjEttizy8',
                'hRgcgcTP7nM',
                'JB0CFw3Godg',
                'FpWubYTW7BA',
                'aYUY0q6dwoY',
                '1GHCYI78ewk',
                'W9P_qUnMaFg',
                'ofmzX1nI7SE',
                '3Qq7kNjg-KY',
                'Px_KohohYT8',
                'o3WdLtpWM_c',
                'OhWqfZPrQ9c',
                'VXgbiugVyxQ',
                '8uOz_IpMBao',
                'p9j8RGTqju0',
                'd3oRyaACTs8',
                'RZYav3Z3wDA',
                'ogUYjxeGkI4',
                'AZAR4GXBgVY',
                'eq3PGCPQOOY'
            ]),
            GenreLinkCollection(genre_name='Classical', song_youtube_ids=[
                'P2l0lbn5TVg',
                'axb48YrvRmw',
                'pIf2zL6aCig',
                'd_92G-Dgscw',
                'QmaF2fvRfhg',
                'y57AWXLMpGU',
                '6-eqYk1j5to',
                'nj2GILDFiE8',
                'CJCx_SCafTg',
                'CssLQR46HAw',
                '9E6b3swbnWg',
                'rrVDATvUitA',
                '3cvmONlV5WU',
                'quxTnEEETbo',
                'Uau48wh5CeI',
                'eY-3rOrxNBE',
                'yc0_qhkZ9iw',
                'jSQrkiSO9DI',
                'GGatJptdRfk',
                '-ro1Z3H_cOg',
                'aC8ejCX_BzU',
                'APNI2CC0k6A',
                'phBThlPTBEg',
                'NqAOGduIFbg',
                'vAeRzW98IFw',
                'EJC-_j3SnXk',
                'YuBeBjqKSGQ',
                'FItvuYc9VRw',
                'afhAqMeeQJk',
                'aJIpVj_YkNo'
            ]),
            GenreLinkCollection(genre_name='Pop', song_youtube_ids=[
                'JGwWNGJdvx8',
                'OPf0YbXqDm0',
                'vZA5heWazIQ',
                'CevxZvSJLk8',
                'RBumgq5yVrA',
                'ZEyAEMA6ojY',
                '4NJH75q0Syk',
                'LOZuxwVk7TU',
                'zpzdgmqIHOQ',
                's__rX_WL100',
                'q3XtRYPbtfI',
                'FrLequ6dUdM',
                'kIDWgqDBNXA',
                'VV1XWJN3nJo',
                'nCzqv0Z5Gjo',
                'bboe5nbYlSs',
                't0bPrt69rag',
                'WQnAxOQxQIU',
                '0UaMsIJ4bR0',
                'pj6FCKm8dhM',
                'XkzEyhFJBHY',
                'RkWQDDv_qdg',
                '4vvBAONkYwI',
                'uESx0DhvYH0',
                'HeK1zQFJtXE',
                '6R9fSfnxSN0',
                'ErntJrtQGBg',
                'Y-JmEmpZ3ts',
                'GP4Cq3VTagU',
                'Xv4NBOWhw9A'
            ])
        ]

    @staticmethod
    def extract_data_for_test():
        return [
            {
                'name': 'Reggae',
                "song_ids": [
                    '-JhwxTen6yA',
                    'S5FCdx7Dn0o'
                ],
            },
            {
                'name': 'Rock',
                "song_ids": [
                    'Lo2qQmj0_h4',
                    'Ckom3gf57Yw'
                ]
            },
            {
                'name': 'Pop',
                "song_ids": [
                    'DEsqGOHo0nI',
                    'p47fEXGabaY'
                ],
            },
            {
                'name': 'Classical',
                "song_ids": [
                    '9Co3ppvdIiE',
                    '0IWRZprH8Zg'
                ],
            },
            {
                'name': 'Electronic',
                "song_ids": [
                    '6mWyhOBNDyg',
                    'nHqe5D4MsCU'
                ]
            }
        ]