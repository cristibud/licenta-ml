class GenreLinkCollection:
    genre_name = None
    song_youtube_ids = []

    def __init__(self, genre_name, song_youtube_ids):
        self.genre_name = genre_name
        self.song_youtube_ids = song_youtube_ids
