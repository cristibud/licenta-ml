import numpy as np
import pandas as pd

from keras.utils import np_utils

class Scaler:
    @staticmethod
    def normalize(data):
        featureName = list(data)
        for name in featureName:
            data[name] = (data[name]-np.min(data[name]))/(np.max(data[name])-np.min(data[name]))
            
        return data

    @staticmethod
    def normalize_data_against_matrix(data, matrix):
        featureName = list(data)
        for name in featureName:
            if (data[name] > np.max(matrix[name])).all():
                data[name] = np.max(matrix[name])
            if (data[name] < np.min(matrix[name])).all():
                data[name] = np.min(matrix[name])
            data[name] = (data[name] - np.min(matrix[name])) / (np.max(matrix[name]) - np.min(matrix[name]))

        return data

    @staticmethod
    def categorical_binary(target):
        return np_utils.to_categorical(target)
    
    @staticmethod
    def transform(data):
        return np.array(data)