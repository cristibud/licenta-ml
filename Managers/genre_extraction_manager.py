import sys

from Infrastructure import scaler
from Infrastructure.reader import Reader
from Resources.FeatureExtraction.feature_extraction_youtube import FeatureExtractorYoutube
from Resources.Classifiers.genre_classifier_predictor import GenreClassifierPredictor
from Resources.Classifiers.genre_classifier_trainer import GenreClassifierTrainer
from InternalDataContracts.static_data_collections import DataCollection
from Infrastructure.scaler import Scaler


class GenreExtractionManager:
    youtube_extractor = None
    model_json_file = "E:/Licenta/Music-Genre-Classification/Storage/Models/model_20_30_TOPBEST/model_20_30.json"
    model_weights_file = "E:/Licenta/Music-Genre-Classification/Storage/Models/model_20_30_TOPBEST/model_20_30.h5"
    training_csv_path = 'E://Licenta//Music-Genre-Classification/Storage/Dataset/training_data_2min.csv'

    def __init__(self):
        self.youtube_extractor = FeatureExtractorYoutube(
            download_path='E:/Licenta/Music-Genre-Classification/FindGenre')

    """downloads the songs and saves a data_frame in the param csv file containing the features extracted for each song
    the csv file also contains the labels associated to each song (rock/ reggae/ etc.)"""

    def extract_data(self, file_name):
        genres_data = DataCollection.extract_data()

        self.youtube_extractor.extract_genres(genres_data=genres_data, clean_download_folder=False)
        self.youtube_extractor.save_df_to_file(file_name)

    def train_model(self, path_to_training_csv, path_to_save_model_to, no_of_epochs, batch_size):
        genre_classifier_trainer = GenreClassifierTrainer()
        return genre_classifier_trainer.train_model(training_csv_path=path_to_training_csv,
                                                    path_to_save_model=path_to_save_model_to, no_of_epochs=no_of_epochs,
                                                    batch_size=batch_size)

    def predict(self, model_json_file_path, model_weights_file_path, song_ids_to_predict, x_matrix):
        predictions_feature_extractor = FeatureExtractorYoutube('E://Licenta//Music-Genre-Classification/temp')
        songs_df = predictions_feature_extractor.extract_songs(song_ids_to_predict)

        scaler = Scaler()
        songs_df_normalized = scaler.normalize_data_against_matrix(songs_df, x_matrix)

        genre_classifier_predictor = GenreClassifierPredictor(model_json_file_path, model_weights_file_path)
        return genre_classifier_predictor.predict_genres(songs_df_normalized)

    def predict_and_evaluate_set(self, json_file_path, h5_file_path, data_with_labels, x_matrix):
        all_song_ids = []
        for dict in data_with_labels:
            for song_id in dict['song_ids']:
                all_song_ids.append((dict['name'], song_id))

        songs_to_predict = [song_id for _, song_id in all_song_ids]
        predictions = self.predict(json_file_path, h5_file_path, songs_to_predict, x_matrix)

        actual_genres = [genre for genre, _ in all_song_ids]
        count = 0
        for index, genre in enumerate(actual_genres):
            if predictions[index] == genre:
                count = count + 1
        score = count * 100 / len(actual_genres)
        return predictions, score

    def predict_youtube_song(self, song_id):
        predictions_feature_extractor = FeatureExtractorYoutube('E://Licenta//Music-Genre-Classification/temp')
        songs_df = predictions_feature_extractor.extract_song(song_id)

        scaler = Scaler()
        reader = Reader()

        data = reader.read_dataset(self.training_csv_path)

        # read the features in a data frame starting from the tempo column
        X = reader.get_features(data, name='tempo')

        songs_df_normalized = scaler.normalize_data_against_matrix(songs_df, X)
        model = GenreClassifierPredictor(self.model_json_file, self.model_weights_file)
        model.predict_genre(songs_df_normalized)

    def main(self, id):
        # Extract Data from Genre Link Collection and save it in csv file
        # self.extract_data('training_data_copy.csv')

        # training_data, json_file_path, h5_file_path = \
        #     self.train_model(self.training_csv_path, "E:/Licenta/Music-Genre-Classification/Storage/Models", 20, 30)
        #
        # data_with_labels = DataCollection.extract_data_for_test()
        # predictions, score = self.predict_and_evaluate_set(json_file_path, h5_file_path, data_with_labels,
        #                                                    training_data)
        # print(predictions)
        # print(score)

        self.predict_youtube_song(id)


genre_ext_mngr = GenreExtractionManager()
genre_ext_mngr.main('c18441Eh_WE')
