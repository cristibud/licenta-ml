import librosa
import numpy as np
import pandas as pd
from os import listdir
from os.path import isfile, join

heading = ['tempo', 'beats', 'chromagram', 'rmse',
           'centroid', 'bandwidth', 'rolloff', 'zcr', 'mfcc1', 'mfcc2',
           'mfcc3', 'mfcc4', 'mfcc5', 'mfcc6', 'mfcc7', 'mfcc8', 'mfcc9',
           'mfcc10', 'mfcc11', 'mfcc12', 'mfcc13', 'mfcc14', 'mfcc15',
           'mfcc16', 'mfcc17', 'mfcc18', 'mfcc19', 'mfcc20']

class FeatureExtractor:
    def __init__(self):
        self.data = None

    def extract(self, path):
        id = 0
        self.data = []
        file_data = [f for f in listdir(path) if isfile(join(path, f))]
        for line in file_data:
            if line[-1:] == '\n':
                line = line[:-1]

            features = []

            id = id + 1
            songname = path + '/' + line

            y, sr = librosa.load(songname, duration=120)
            tempo, beats = librosa.beat.beat_track(y=y, sr=sr)
            chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
            rmse = librosa.feature.rmse(y=y)
            cent = librosa.feature.spectral_centroid(y=y, sr=sr)
            spec_bw = librosa.feature.spectral_bandwidth(y=y, sr=sr)
            rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr)
            zcr = librosa.feature.zero_crossing_rate(y)
            mfcc = librosa.feature.mfcc(y=y, sr=sr)

            features.append(tempo)
            features.append(np.sum(beats))
            features.append(np.mean(chroma_stft))
            features.append(np.mean(rmse))
            features.append(np.mean(cent))
            features.append(np.mean(spec_bw))
            features.append(np.mean(rolloff))
            features.append(np.mean(zcr))
            for coefficient in mfcc:
                features.append(np.mean(coefficient))

            self.data.append(features)

    def get_data_as_df(self):
        df = pd.DataFrame(self.data, columns=heading)
        return df

    def save_df_to_file(self, file_name):
        self.get_data_as_df().to_csv(file_name)
