import os
import pafy
import pandas as pd
from shutil import rmtree

from Resources.FeatureExtraction.feature_extraction import FeatureExtractor
from InternalDataContracts.genre_link_collection import GenreLinkCollection


class FeatureExtractorYoutube:
    data_frame = None
    download_path = None
    GENRES_LIST = ["Blues", "Classical", "Country", "Disco", "Hip Hop", "Jazz", "Metal", "Pop", "Reggae", "Rock"]

    def __init__(self, download_path="E:/Licenta/Music-Genre-Classification/FindGenre"):
        self.data_frame = None
        self.download_path = download_path

    def extract_genres(self, genres_data, clean_download_folder=False):
        genres_dfs = []

        # Delete the contents of the download folder if specified
        if clean_download_folder is True:
            self.__clean_download_folder()

        # Extract a df for each genre and append it to the genres_df list
        for genre_data in genres_data:
            df = self.__extract_genre(genre_data)
            genres_dfs.append(df)

        # Create the concatenated df
        self.data_frame = pd.concat(genres_dfs)

    def extract_songs(self, song_ids):

        self.__clean_download_folder()

        if not os.path.exists(self.download_path):
            os.makedirs(self.download_path)

        for index, song_id in enumerate(song_ids):
            # Download the song
            url = "https://www.youtube.com/watch?v=" + song_id
            video = pafy.new(url)
            bestAudio = video.getbestaudio()
            file_full_path = f'{self.download_path}/temp.{index}'
            bestAudio.download(filepath=file_full_path)

        # Extract the song features
        extractor = FeatureExtractor()
        extractor.extract(self.download_path)
        genre_df = extractor.get_data_as_df()

        self.data_frame = genre_df

        self.__clean_download_folder()

        return genre_df

    def extract_song(self, song_id):

        # Download the song
        url = "https://www.youtube.com/watch?v=" + song_id
        video = pafy.new(url)
        bestAudio = video.getbestaudio()
        file_full_path = f'{self.download_path}/temp'
        bestAudio.download(filepath=file_full_path)

        # Extract the song features
        extractor = FeatureExtractor()
        extractor.extract(self.download_path)
        genre_df = extractor.get_data_as_df()

        self.data_frame = genre_df

        self.__clean_download_folder()

        return genre_df

    def save_df_to_file(self, file_name):
        self.data_frame.to_csv(file_name)

    def __extract_genre(self, genre_data: GenreLinkCollection):
        path = f'{self.download_path}/{genre_data.genre_name}'

        if not os.path.exists(path):
            os.makedirs(path)

        for index, song_id in enumerate(genre_data.song_youtube_ids):
            # Download the song
            url = "https://www.youtube.com/watch?v=" + song_id
            video = pafy.new(url)
            bestAudio = video.getbestaudio()
            file_full_path = f'{path}/{genre_data.genre_name}.{index}'
            bestAudio.download(filepath=file_full_path)

        # Extract the song features
        extractor = FeatureExtractor()
        extractor.extract(path)
        genre_df = extractor.get_data_as_df()

        # Assign the genre and song_id to the genre_df (at the first indexes)
        # genre_index = [i for i, genre in enumerate(FeatureExtractorYoutube.GENRES_LIST) if genre is genre_data.genre_name][0]

        genre_df.insert(loc=0, column="genre", value=[0] * len(genre_data.song_youtube_ids))
        genre_df.insert(loc=0, column="song_id", value=genre_data.song_youtube_ids)

        # Return the data frame
        return genre_df

    """Remove the files stored in the download path"""

    def __clean_download_folder(self):
        for filename in os.listdir(self.download_path):
            path = os.path.join(self.download_path, filename)
            if os.path.isfile(path):
                os.remove(path)
            else:
                rmtree(path)
