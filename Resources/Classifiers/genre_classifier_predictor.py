from keras.engine.saving import model_from_json
import numpy as np


class GenreClassifierPredictor(object):
    GENRES_LIST = ["Reggae", "Rock", "Electronic", "Classical", "Pop"]

    def __init__(self, model_json_file, model_weights_file):
        # load model from JSON file
        with open(model_json_file, "r") as json_file:
            loaded_model_json = json_file.read()
            self.loaded_model = model_from_json(loaded_model_json)

        self.loaded_model.load_weights(model_weights_file)
        self.loaded_model.summary()

    def predict_genres(self, songs_dfs):
        predictions = []

        no_of_rows = songs_dfs.shape[0]

        for i in list(range(no_of_rows)):
            song_df = songs_dfs.ix[i:i, :]
            prediction = self.predict_genre(song_df)
            predictions.append(prediction)

        return predictions

    def predict_genre(self, song_df):
        self.preds = self.loaded_model.predict(song_df, steps=128)
        print(GenreClassifierPredictor.GENRES_LIST[np.argmax(self.preds)])
        return GenreClassifierPredictor.GENRES_LIST[np.argmax(self.preds)]
