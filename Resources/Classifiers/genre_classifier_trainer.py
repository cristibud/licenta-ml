import os
from keras.layers import Dense, Dropout
from keras.models import Sequential
from matplotlib import pyplot as plt

from Infrastructure.reader import Reader
from Infrastructure.scaler import Scaler

import pandas as pd
import random


class GenreClassifierTrainer:
    reader = None
    scaler = None

    def __init__(self):
        self.reader = Reader()
        self.scaler = Scaler()

    def plot_model_accuracy(self, model):
        plt.plot(model.history['acc'])
        plt.plot(model.history['val_acc'])
        plt.title('Model Accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['Training Accuracy', 'Evaluation Accuracy'], loc='upper left')
        plt.show()

    def plot_model_loss(self, model):
        plt.plot(model.history['loss'])
        plt.plot(model.history['val_loss'])
        plt.title('Model Loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['Training Loss', 'Evaluation Loss'], loc='upper left')
        plt.show()

    def train_model(self, training_csv_path, path_to_save_model, no_of_epochs=30, batch_size=128):
        data = self.reader.read_dataset(training_csv_path)

        # read the features in a data frame starting from the tempo column
        X = self.reader.get_features(data, name='tempo')
        X_copy = X.copy()
        # read the labels associated to each song
        y = self.reader.get_label(data, name='genre')

        X_normalized = self.scaler.normalize(X)

        # Creating a Neural Networks Model
        model = Sequential()
        model.add(Dense(28, input_shape=(X.shape[1],), activation='relu'))
        model.add(Dense(256, activation='relu'))
        model.add(Dropout(0.25))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(2048, activation='relu'))
        model.add(Dense(5, activation='softmax'))

        # Compiling Neural Networks Model
        model.compile(loss='categorical_crossentropy',
                      optimizer="adam",
                      metrics=["acc"])

        # Split data in training and test data
        x_train, y_train, x_test, y_test = self.split_into_training_and_test(X_normalized, y)

        # Train the model
        y_train_categorical = self.scaler.categorical_binary(y_train)
        y_test_categorical = self.scaler.categorical_binary(y_test)

        X_trainMatrix = self.scaler.transform(x_train)
        results = model.fit(x_train, y_train_categorical,
                            validation_data=(x_test, y_test_categorical),
                            epochs=no_of_epochs, batch_size=batch_size)

        # Plot accuracy and loss training
        # self.plot_model_accuracy(results)
        # self.plot_model_loss(results)

        # Save the model
        json_path, weights_path = self.__save_model(model, path_to_save_model, f'model_{no_of_epochs}_{batch_size}')
        return X_copy, json_path, weights_path

    def split_into_training_and_test(self, X, Y, split_percentage=0.2, number_of_items_per_genre=30) -> tuple:
        # stiu ca din 10 in 10 tot schimb genul muzical -> asta e fix
        no_of_genres = int(X.shape[0] / number_of_items_per_genre)

        no_of_items_for_test = int(split_percentage * number_of_items_per_genre)

        X_train_dfs = []
        Y_train_dfs = []
        X_test_dfs = []
        Y_test_dfs = []

        for i in list(range(no_of_genres)):
            prev_count = i * number_of_items_per_genre
            X_df = X.iloc[prev_count:prev_count + number_of_items_per_genre, :]
            Y_df = Y.iloc[prev_count:prev_count + number_of_items_per_genre]

            random_indexes_for_train = random.sample(range(number_of_items_per_genre), no_of_items_for_test)

            # sort the indexes reversed to make sure they are still there after manipulating the dfs
            random_indexes_for_train.sort(reverse=True)

            for index in random_indexes_for_train:
                X_test = X_df.iloc[index:index + 1, :]
                Y_test = Y_df.iloc[index:index + 1]
                X_test_dfs.append(X_test)
                Y_test_dfs.append(Y_test)

                X_df = X_df.drop([prev_count + index])
                Y_df = Y_df.drop([prev_count + index])

            X_train_dfs.append(X_df)
            Y_train_dfs.append(Y_df)

        return pd.concat(X_train_dfs), pd.concat(Y_train_dfs), pd.concat(X_test_dfs), pd.concat(Y_test_dfs)

    def __save_model(self, model, model_dir, model_name):
        path = f'{model_dir}/{model_name}'
        if not os.path.exists(path):
            os.makedirs(path)

        model_json = model.to_json()
        json_file_path = f'{path}/{model_name}.json'
        with open(json_file_path, "w") as json_file:
            json_file.write(model_json)

        weights_file_path = f'{path}/{model_name}.h5'
        model.save_weights(weights_file_path)
        return json_file_path, weights_file_path
